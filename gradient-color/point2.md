#CSS gradients

## Introduction

La fonctionnalité gradient de CSS3 permet d'effectuer des transitions entre différentes couleurs sous forme de "fondu de couleurs"

[Référence W3C](http://www.w3schools.com/css/css3_gradients.asp)

* * *

## Intérêt

Le module gradient de css est utilisé pour réaliser des transitions entre différentes couleurs. Cela permet d'éviter d'utiliser des images ce qui accèlère le temps de chargement de la page

* * *

## Utilisation

Le module gradient s'applique sur le background. Il existe deux types de propriété gradient : linear et radial.

* * *

### Linear :

Le prototype est le suivant : **background: linear-gradient(direction, color-stop1, color-stop2, ...);**  
La direction peut être défini de plusieurs manières: de haut en bas, de bas en haut, de gauche à droite, de droite à gauche et diagonalement.  
Il est aussi possible de définir plusieurs couleurs afin d'affiner la transition.  
Le module intégre aussi la transparence, pour cela il faut utiliser définir les couleurs avec la fonction rgba et indiquer une transparence.  
Il est aussi possible de répéter le dégradé avec la fonction repeating-linear-gradient, qui s'utilise de la même façon.  

* * *

### Radial:

Le prototype est le suivant : **background: radial-gradiant(shape size at position, start-color, ...., last-color);**
Contrairement au linear, le dégradé du radial commence à partir d'un point qui sera utilisé comme milieu référent.  
Le paramètre shape permet de définir la forme du dégradé il peut prendre deux valeurs : circle ou ellipse.  
Comme pour le linear il est possible de répété le radial avec la fonction repeating-radial-gradient.

* * *

## Implatation sur navigateur

Vis à vis de l'implantation sur les différents navigateurs, il peut y avoir des problèmes avec les versions d'Internet Explorer antérieures à la version 10, mais il reste compatible avec tout les autres navigateurs.
En utilisant le site ColorZilla pour la génération des gradient, il est possible d'éviter ces problèmes puisque nous pouvons indiquer lors des choix des couleurs que nous sommes sur Internet Explorer.

* * *

## Documentation

Il existe donc des sites qui permettent de découvrir ce que sont les gradients et de les assimiler à travers des exercices. Il existe également des sites pour pouvoir générer les gradients avec les couleurs que nous voulons.  
Voici quelques sites qui fourniront les ressources nécessaires pour une bonne appréhension de la fonctionnalité radient:

1.  [Pour apprendre les bases](https://developer.mozilla.org/fr/docs/Web/CSS/linear-gradient)
2.  [Pour manipuler les gradients](https://developer.mozilla.org/fr/docs/Web/CSS/Utilisation_de_d%C3%A9grad%C3%A9s_CSS)
3.  [Pour générer des gradients à partir des couleurs choisies par l'utilisateur](http://www.colorzilla.com/gradient-editor/)

* * *

#CSS color module

##Introduction

Le type de données CSS <color> permet de représenter des couleurs dans l'espace de couleurs RGB (Red, Green, Blue).

[Référence W3C](http://www.w3schools.com/colors/default.asp)

* * *

## Intérêt

L'intérêt du module de couleur est de modifier la couleur des éléments

* * *

## Utilisation

Le module color peut-être appliquer sur des éléments comme le texte ou le background.

Il existe deux façon de définir la couleur voulue: avec la méthode RGB ou la définition hexadecimal

* * *

### RGB Color

Le rgb color consiste à passer en paramètre de la méthode rgb() les valeurs décimales des nuances rouge, verte ou bleu comprises entre 0 et 255.  
Prototype de la fonction:

*   **color: rgb(255,0,0);** Rouge
*   **color: rgb(0,255,0);** Vert
*   **color: rgb(0,0,255);** Bleu

#### La méthode rgba - Ajouter une transparence

En utilisant la méthode rgba() au lieu de la méthode rgb() il est possible d'ajouter directement une transparence.  
Prototype de la fonction:

*   **color: rgba(255,0,0,0);** Rouge avec transparence
*   **color: rgba(255,0,0,1);** Rouge sans transparence

* * *

### Hexadecimal Color

L'hexadecimal color consiste à donner la couleur en héxadécimal, la méthode rgb() n'est pas utilisé.  
Prototype de la fonction:

*   **color: #FF0000;** Rouge
*   **color: #00FF00;** Vert
*   **color: #0000FF:** Bleu

* * *

## Implantation sur la navigateur

L'implatation du module de couleur est disponible sur tous les navigateurs

* * *

## Documentation

Il existe de nombreux sites qui permettent d'apprendre à maitriser facilement les propriétés du module de couleur.
Voici quelques sites utiles :

* 1. [Documentation de base en français](https://developer.mozilla.org/fr/docs/Web/CSS/Type_color)
* 2. [Documentation W3C plus avancée](https://www.w3.org/TR/css-color-4/)
* 3. [Documentaion W3School](http://www.w3schools.com/colors/colors_rgb.asp)
